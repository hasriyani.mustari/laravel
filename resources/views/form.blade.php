<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form</title>
</head>
<body>
    <h2>Buat Account Baru!</h2>
    <h4>Sign Up Form</h4>
    <form action="/welcome" method="POST">
        @csrf
        <label>First Name:</label><br><br>
        <input type="text" name='first_name'> <br><br>
        <label>Last Name:</label><br><br>
        <input type="text" name='last_name'> <br><br>
        <label>Gender:</label><br><br>
        <input type="radio" name="gender">Male <br>
        <input type="radio" name="gender">Female <br>
        <input type="radio" name="gender">Other <br><br>
        <label>Nasionality</label><br><br>
        <select name="country" id="">
            <option>Indonesian</option>
            <option>Singaporean</option>
            <option>Malaysian</option>
            <option>Australian</option>
        </select>
        <br><br>
        <label>Language Spoken:</label><br><br>
        <input type="checkbox" name="language">Bahasa Indonesia <br>
        <input type="checkbox" name="language">English <br>
        <input type="checkbox" name="language">Other <br>
        <br>
        <label>Bio</label><br><br>
        <textarea cols="30" rows="10"></textarea><br>
        <input type="submit" value="Sign Up">
    </form>
</body>
</html>